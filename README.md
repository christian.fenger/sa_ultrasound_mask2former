# SA_Ultrasound_Mask2Former

Please execute all commands in the given order!
A successful installation also depends on the correct versions of all packages. This guide is just made for the given version numbers. There is no guarantee that it also works, like it is described here, with different versions!

## Installation
### Cluster
`module load CUDA/11.3.1` \
`module load GCC/9.4.0`

### Conda
`conda create --name mask2former python=3.8 -y` \
`conda activate mask2former`

`cd <desired directory>`

### Python
`pip install torch==1.10.0 torchvision==0.11.0 torchaudio --index-url https://download.pytorch.org/whl/cu113` \
`pip install -U opencv-python`

### Detectron2
`git clone https://github.com/facebookresearch/detectron2.git` \
`cd detectron2` \
`pip install -e .` \
`pip install git+https://github.com/cocodataset/panopticapi.git` \
`pip install git+https://github.com/mcordts/cityscapesScripts.git`

### Mask2Former
`cd ..` \
`git clone https://github.com/facebookresearch/Mask2Former.git` \
`cd Mask2Former` \
`pip install -r requirements.txt` \
`cd mask2former/modeling/pixel_decoder/ops` \
`sh make.sh`

If installation through make.sh doesn't work ... \
`pip install build` \
`python setup.py build install`

### Errors
AttributeError: module 'distutils' has no attribute 'version' \
	-> `python3 -m pip install setuptools==59.5.0`

Most of the other errors are version errors. It is very important to have a look which versions are compatible with each other

**For more, see ...** \
[Installation - Mask2Former](https://github.com/facebookresearch/Mask2Former/blob/main/INSTALL.md) \
[Getting Started - Mask2Former](https://github.com/facebookresearch/Mask2Former/blob/main/GETTING_STARTED.md)

## Python Scripts
### Relevant
**main.py**: Script which can be called from console to execute registration, training, evaluation, mean inference time calculation \
**UltrasoundTrainer.py**: Consists of configuration, training execution, evaluation, visualizations
**RegisterDataset.py**: Registers dataset in architecture/network. Contains custom data loader

### Optional
**convertGroundTruth.py**: Normlaizes mask image pixels \
**dilation.py**: Dilates mask images \
**sortImagesByPatient.py**: Creates folders each patient and sorts all images into corresponding folders 

## Shell Scripts
**train_ultrasound.py**: Used to trigger training
**inferenceTime.sh**: Extra script for mean inference time calculation only