#!/bin/sh

#SBATCH -J gpu_serial
#SBATCH -o gpu_serial.%J.log
#SBATCH --gres=gpu:1

#SBATCH --job-name=train_ultrasound

#SBATCH --time=1-00:00              			# Runtime in D-HH:MM
#SBATCH --output=output.%J.log         			# File to which STDOUT will be written
#SBATCH --mail-type=ALL         			# Type of email notification- BEGIN,END,FAIL,ALL
#SBATCH --mail-user=christian.fenger@rwth-aachen.de  

#module load CUDA

# Print some debug information
#echo; export; echo; nvidia-smi; echo
#nvidia-smi

# Activate environment
source anaconda3/bin/activate
conda activate mask2former
cd Meditec/Mask2Former

# Execute script
python main.py -meanInferenceTime
