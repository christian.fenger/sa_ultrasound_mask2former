from RegisterDataset import RegisterDataset
from UltrasoundTrainer import UltrasoundTrainer
import sys

# Main file to execute training, prediction, evaluation, inference time calculation and verification
def main(datasetsBasePath: str):
    print("Start register datasets at path",datasetsBasePath, "...")

    #Dataset registration is mandatory!!
    registerDataset = RegisterDataset()
    registerDataset.register(datasetsBasePath)

    print("Done!")

# Executes training
def train():
    trainer = UltrasoundTrainer()
    trainer.train()

# Executes prediction
def predict():
    trainer = UltrasoundTrainer()
    trainer.predict()

# Executes prediction of all images. Shows them one by one in a sequence
def predictAll():
    trainer = UltrasoundTrainer()
    trainer.predictAll()

# Executes evaluation
def evaluate():
    trainer = UltrasoundTrainer()
    trainer.evaluate()

# Executes mean inference time calculation
def meanInferenceTime():
    trainer = UltrasoundTrainer()
    trainer.meanInferenceTime()

# Executes verification
def verify(basePathTrain, datasetName):
    # /home/cf373976/Meditec/Mask2Former/dataset/CARS2/
    registerDataset = RegisterDataset()
    registerDataset.verifyRegistration(basePathTrain, datasetName)


if __name__ == '__main__':
    args = sys.argv
    print("Parameters: ", args)

    if len(args) == 2:
        if args[1] == '-test':
            print("Execute test")
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            verify("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/", "ultrasound_train_BL")
            print("Test done")

    if len(args) == 3:
        if args[1] == '-register':
            main(args[2])
        else:
            print("Arguments -register <path> are required")
            
    if len(args) == 2:
        if args[1] == '-train':
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            train()
        if args[1] == '-predict':
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            predict()
        if args[1] == '-predictAll':
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            predictAll()
        if args[1] == '-evaluate':
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            evaluate()
        if args[1] == '-meanInferenceTime':
            main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/")
            meanInferenceTime()
    
    if len(args) == 4:
        if args[1] == '-verify':
            verify(args[2], args[3])
