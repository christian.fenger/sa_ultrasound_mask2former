import cv2
import os

#Script which was used to dilate the mask images. It overwrites the original mask image!!
def main(masksBasePath: str):
   
    #get all names of datasets (patient abbreviation -> directory names)
    datasetNames = os.listdir(masksBasePath) 

    #Iterate through mask all images of datasets to dilate the image
    for dataset in datasetNames:
        maskImageNames = os.listdir(os.path.join(masksBasePath, dataset)) 
        for maskImageName in maskImageNames:
            image = cv2.imread(os.path.join(masksBasePath, dataset, maskImageName),0)
            newImage = dilateAnnotation(image)
			
            #This overwrites the used mask image!
            cv2.imwrite(os.path.join(masksBasePath, dataset, maskImageName), newImage)

    print("Done!")

#Method which executes the dilation of one image
def dilateAnnotation(image):
    kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
    return cv2.dilate(image, kernel, iterations=5)

if __name__ == "__main__":
    main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/masks")
