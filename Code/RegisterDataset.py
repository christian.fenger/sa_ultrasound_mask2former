import os
import cv2
import pycocotools
import numpy as np
import matplotlib.pyplot as plt
import detectron2
from detectron2.structures import BoxMode
import random
from detectron2.utils.visualizer import Visualizer
from detectron2.utils.visualizer import ColorMode
from detectron2.data import MetadataCatalog, DatasetCatalog

# Registers the given datasets in the system. This is mandatory to use the architecture
class RegisterDataset:

#Only necessary for instance segmentation
    '''
    def load_bitmask_image(self, img_dir, filename):
        #Load mask image
        maskImage = cv2.imread(os.path.join(img_dir, filename), 0)

        #Extract bitmask
        bitMask = []
        hasMask = False
        rows,cols = maskImage.shape
        for i in range(rows):
            row = []
            for j in range(cols):
                val = maskImage[i,j]
                if val == 255:
                    row.append(1)
                elif val == 1:
                    row.append(1)
                elif val == 0:
                    row.append(0)
                else:
                    #TODO What should be do if mask file is not in the correct format? Validate before?
                    row.append(0)
            bitMask.append(row)
        return bitMask


    def get_box_points(self, bitmask):
        top = -1
        right = -1
        bottom = -1
        left = -1

        rowIdx = 0
        for row in bitmask:
            colIdx = 0
            for col in row:
                if col == 1:
                    #Top border: lowest row index (y-coordinate)
                    if rowIdx < top or top == -1:
                        top = rowIdx
                    #Right border: highest column index (x-coordinate)
                    if colIdx > right or right == -1:
                        right = colIdx
                    #Bottom border: highest row index (y-coordinate)
                    if rowIdx > bottom or bottom == -1:
                        bottom = rowIdx
                    #Left border: lowest column index (x-coordinate)
                    if colIdx < left or left == -1:
                        left = colIdx

                colIdx += 1
            rowIdx += 1

        #Return coordinates (left, top) and (right, bottom) for the bounding box
        return [left, top, right, bottom]
    '''

    # Custom data loader which is registered together with the datasets
    def get_ultrasound_dicts(self, img_dir, datasetName):
        print("Input: ", img_dir + "" +datasetName)
        #Return object
        dataset_dicts = []

        #Get file names of original and mask images and sort them. It is important to keep the order to use the image context in the training later 
        datasetImageDirPath = os.path.join(img_dir, 'images', datasetName)
        if not os.path.exists(datasetImageDirPath):
            print("Images path does not exist", datasetImageDirPath)
            return dataset_dicts
        img_fileNames = os.listdir(datasetImageDirPath)
        img_fileNames.sort()
        datasetMaskDirPath = os.path.join(img_dir, 'masks', datasetName)
        if not os.path.exists(datasetMaskDirPath):
            print("Masks path does not exist", datasetMaskDirPath)
            return dataset_dicts
        mask_fileNames = os.listdir(datasetMaskDirPath)
        mask_fileNames.sort()

        # Iterate through each image and create the document which will be used in the network to train on semantic segmentation
        idx = 0
        for img_fileName in img_fileNames:
            #print('',idx,'- Processing', img_fileName)
            maskFileName = mask_fileNames[idx]

            record = {}

            # Image meta data
            filePath = os.path.join(datasetImageDirPath, img_fileName)
            image = cv2.imread(filePath, 0)
            height, width = image.shape
            record["file_name"] = os.path.join(datasetImageDirPath, img_fileName)
            record["image_id"] = idx
            record["height"] = height
            record["width"] = width
            
            # Path to mask image file
            record["sem_seg_file_name"] = os.path.join(datasetMaskDirPath, maskFileName)

            # Only needed for instance segmentation
            '''
            anno_bitmask = self.load_bitmask_image(datasetMaskDirPath, maskFileName)
		    #Keep the list for the case we can have multiple annotations
       	    objs = []
       	    coco_encoded_bitmask = pycocotools.mask.encode(np.asarray(anno_bitmask, dtype=np.uint8, order="F"))

       	    obj = {
       	        "bbox": self.get_box_points(anno_bitmask),
       	        "bbox_mode": BoxMode.XYXY_ABS,
       	        "segmentation": coco_encoded_bitmask,
       	        "category_id": 0,
       	    }
       	    objs.append(obj)
       	    record["annotations"] = objs
            '''

            '''
            if not anno_bitmask is None:
                dataset_dicts.append(record)
                print("Add", record["file_name"])
       	    '''
            dataset_dicts.append(record)

            idx += 1
            #if len(dataset_dicts) == 25:
            #    break
        return list(dataset_dicts)
    
    # Registers all datasets seperately. All together in a loop, like in the tutorial, didn't worked somehow ... 
    def register(self, basePath: str):

        class_names = ["", "bone"] # ["background", "bone"]
        class_ids = class_names # ["0","1"]
        stuff_colors = [( 0, 0, 0), ( 0, 0, 255)]
        stuff_id = {}
        stuff_id[0] = 0
        stuff_id[1] = 1
        
        # Registrations of all datasets in the DatasetCatalog and the MetadataCatalog of detectron2
        DatasetCatalog.register("ultrasound_train_BL", lambda: self.get_ultrasound_dicts(basePath, "BL"))
        MetadataCatalog.get("ultrasound_train_BL").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_DR", lambda: self.get_ultrasound_dicts(basePath, "DR"))
        MetadataCatalog.get("ultrasound_train_DR").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_DW", lambda: self.get_ultrasound_dicts(basePath, "DW"))
        MetadataCatalog.get("ultrasound_train_DW").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_IV", lambda: self.get_ultrasound_dicts(basePath, "IV"))
        MetadataCatalog.get("ultrasound_train_IV").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_KG", lambda: self.get_ultrasound_dicts(basePath, "KG"))
        MetadataCatalog.get("ultrasound_train_KG").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_MM", lambda: self.get_ultrasound_dicts(basePath, "MM"))
        MetadataCatalog.get("ultrasound_train_MM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_MP", lambda: self.get_ultrasound_dicts(basePath, "MP"))
        MetadataCatalog.get("ultrasound_train_MP").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_MS", lambda: self.get_ultrasound_dicts(basePath, "MS"))
        MetadataCatalog.get("ultrasound_train_MS").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_MY", lambda: self.get_ultrasound_dicts(basePath, "MY"))
        MetadataCatalog.get("ultrasound_train_MY").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_RK", lambda: self.get_ultrasound_dicts(basePath, "RK"))
        MetadataCatalog.get("ultrasound_train_RK").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_SA", lambda: self.get_ultrasound_dicts(basePath, "SA"))
        MetadataCatalog.get("ultrasound_train_SA").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_SM", lambda: self.get_ultrasound_dicts(basePath, "SM"))
        MetadataCatalog.get("ultrasound_train_SM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_TS", lambda: self.get_ultrasound_dicts(basePath, "TS"))
        MetadataCatalog.get("ultrasound_train_TS").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_train_WM", lambda: self.get_ultrasound_dicts(basePath, "WM"))
        MetadataCatalog.get("ultrasound_train_WM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)


        DatasetCatalog.register("ultrasound_val_BL", lambda: self.get_ultrasound_dicts(basePath, "BL"))
        MetadataCatalog.get("ultrasound_val_BL").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_DR", lambda: self.get_ultrasound_dicts(basePath, "DR"))
        MetadataCatalog.get("ultrasound_val_DR").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_DW", lambda: self.get_ultrasound_dicts(basePath, "DW"))
        MetadataCatalog.get("ultrasound_val_DW").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_IV", lambda: self.get_ultrasound_dicts(basePath, "IV"))
        MetadataCatalog.get("ultrasound_val_IV").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_KG", lambda: self.get_ultrasound_dicts(basePath, "KG"))
        MetadataCatalog.get("ultrasound_val_KG").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_MM", lambda: self.get_ultrasound_dicts(basePath, "MM"))
        MetadataCatalog.get("ultrasound_val_MM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_MP", lambda: self.get_ultrasound_dicts(basePath, "MP"))
        MetadataCatalog.get("ultrasound_val_MP").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_MS", lambda: self.get_ultrasound_dicts(basePath, "MS"))
        MetadataCatalog.get("ultrasound_val_MS").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_MY", lambda: self.get_ultrasound_dicts(basePath, "MY"))
        MetadataCatalog.get("ultrasound_val_MY").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_RK", lambda: self.get_ultrasound_dicts(basePath, "RK"))
        MetadataCatalog.get("ultrasound_val_RK").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_SA", lambda: self.get_ultrasound_dicts(basePath, "SA"))
        MetadataCatalog.get("ultrasound_val_SA").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_SM", lambda: self.get_ultrasound_dicts(basePath, "SM"))
        MetadataCatalog.get("ultrasound_val_SM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_TS", lambda: self.get_ultrasound_dicts(basePath, "TS"))
        MetadataCatalog.get("ultrasound_val_TS").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)
        DatasetCatalog.register("ultrasound_val_WM", lambda: self.get_ultrasound_dicts(basePath, "WM"))
        MetadataCatalog.get("ultrasound_val_WM").set(stuff_colors=stuff_colors, 
                                             stuff_classes=class_ids, 
                                             evaluator_type=["sem_seg"],
                                             stuff_dataset_id_to_contiguous_id=stuff_id,
                                             ignore_label=[0],)

    # Creates a visualitation for a given image 
    def visualize(self, **images):
        """
        Plot images in one row
        """
        n_images = len(images)
        plt.figure(figsize=(20,8))
        for idx, (name, image) in enumerate(images.items()):
            plt.subplot(1, n_images, idx + 1)
            plt.xticks([]); 
            plt.yticks([])
            # get title from the parameter names
            plt.title(name.replace('_',' ').title(), fontsize=20)
            plt.imshow(image)
        plt.show()
    
    # Registers a dataset and visualizes a random image with it's mask (just for testing and playing around)
    def verifyRegistration(self, basePath, datasetName: str):
        #To verify if everything is in the correct format
        ultrasound_metadata = MetadataCatalog.get(datasetName)
        if not ultrasound_metadata is None:
            print("Metadata loaded: ", datasetName)
        print("Try to load and visualize dataset", datasetName, "from path", basePath)

        dataset_dicts = DatasetCatalog.get(datasetName)
        if len(dataset_dicts) == 0:
            print("Requested dataset not found in catalog:", datasetName)
            return

        print('Dataset_dict size:', len(dataset_dicts))

        d = random.choice(list(dataset_dicts))
        print(d["file_name"])
        img = cv2.imread(d["file_name"])
        print(d["sem_seg_file_name"])
        mask = cv2.imread(d["sem_seg_file_name"])

        visualizer = Visualizer(img[:, :, ::-1], metadata=ultrasound_metadata, scale=1.5, instance_mode=ColorMode.SEGMENTATION)
        out = visualizer.draw_dataset_dict(d)

        self.visualize(
            original_image = out.get_image()[:, :, ::-1],
            mask = mask
        )

        #cv2.imshow('Visualized Result', out.get_image()[:, :, ::-1])
        #cv2.waitKey(5000)

        # Used for showing multiple random images after another
        '''
        for d in random.sample(list(dataset_dicts), 1):

            img = cv2.imread(d["file_name"])

            visualizer = Visualizer(img[:, :, ::-1], metadata=ultrasound_metadata, scale=1.5, instance_mode=ColorMode.SEGMENTATION)

            out = visualizer.draw_dataset_dict(d)
            print(d["file_name"])
            cv2.imshow('Visualized Result', out.get_image()[:, :, ::-1])
            cv2.waitKey(5000)
        '''
