import cv2
import os
import numpy as np

#Script to normalize the image pixels. In this case, the pixel value of 255 will be converted to 1
def main(masksBasePath: str):
   
    #Iterate through all datasets and normalize all mask images
    datasetNames = os.listdir(masksBasePath) 
    for dataset in datasetNames:
        maskImageNames = os.listdir(os.path.join(masksBasePath, dataset)) 
        for maskImageName in maskImageNames:
            image = np.array(cv2.imread(os.path.join(masksBasePath, dataset, maskImageName),0))
            #image[image == 1] = 255
            image[image == 255] = 1
            cv2.imwrite(os.path.join(masksBasePath, dataset, maskImageName), image, [cv2.IMWRITE_PNG_COMPRESSION, 0])

    print("Done!")

if __name__ == "__main__":
    main("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/masks")
