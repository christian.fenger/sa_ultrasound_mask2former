from detectron2.engine import DefaultTrainer
from detectron2.engine import DefaultPredictor
from detectron2.config import get_cfg
from detectron2 import model_zoo
from detectron2.utils.visualizer import Visualizer
from detectron2.data import MetadataCatalog, DatasetCatalog
from detectron2.utils.visualizer import ColorMode
from detectron2.evaluation import SemSegEvaluator, inference_on_dataset
from detectron2.data import build_detection_test_loader
import os
import cv2
import matplotlib.pyplot as plt
import torch
import time

# Trainer class. Executes training and creates visualizations of predictions
class UltrasoundTrainer():

    # Create configuration for training
    def createConfiguration(self):
        cfg = get_cfg()
        cfg.merge_from_file("/home/cf373976/Meditec/Mask2Former/detectron2/configs/Base-RCNN-FPN.yaml")
        cfg.DATASETS.TRAIN = ("ultrasound_train_BL", "ultrasound_train_DR","ultrasound_train_DW","ultrasound_train_IV","ultrasound_train_KG","ultrasound_train_MM","ultrasound_train_MP","ultrasound_train_MS","ultrasound_train_MY","ultrasound_train_RK","ultrasound_train_SA",)
        cfg.DATASETS.TEST = ("ultrasound_val_TS", "ultrasound_val_WM",)
        cfg.DATALOADER.NUM_WORKERS = 1
        cfg.MODEL.META_ARCHITECTURE = "SemanticSegmentor"
        cfg.INPUT.MASK_FORMAT = "bitmask" 
        cfg.SOLVER.IMS_PER_BATCH = 1 
        cfg.SOLVER.BASE_LR = 0.0001
        cfg.SOLVER.MAX_ITER = 30000 
        cfg.SOLVER.STEPS = []
        cfg.MODEL.ROI_HEADS.BATCH_SIZE_PER_IMAGE = 64
        cfg.MODEL.ROI_HEADS.NUM_CLASSES = 2
        #cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5  # set threshold for this model
        cfg.MODEL.SEM_SEG_HEAD.NUM_CLASSES = 2
        cfg.OUTPUT_DIR = "/home/cf373976/Meditec/Mask2Former/Output_1_1"
        cfg.OUTPUT_EVAL = "/home/cf373976/Meditec/Mask2Former/Evaluation_1_1"

        return cfg
    
    # Executes training based on configuration returned by configuration creation function
    def train(self):
        cfg = self.createConfiguration()
        os.makedirs(cfg.OUTPUT_DIR, exist_ok=True)
        print("Write output to ", cfg.OUTPUT_DIR)
        trainer = DefaultTrainer(cfg) 
        trainer.resume_or_load(resume=False)
        trainer.train()

    # Plot images in one row
    def visualize(self, **images):
        n_images = len(images)
        plt.figure(figsize=(20,8))
        for idx, (name, image) in enumerate(images.items()):
            plt.subplot(1, n_images, idx + 1)
            plt.xticks([]); 
            plt.yticks([])
            # get title from the parameter names
            plt.title(name.replace('_',' ').title(), fontsize=20)
            plt.imshow(image)
        plt.show()

    # Makes prediction and creates visualization of a the given image
    def predictSingleImage(self, dataset: str, datasetAbre: str, imageName: str):
        cfg = self.createConfiguration()
        cfg.MODEL.WEIGHTS = os.path.join("/home/cf373976/Meditec/Mask2Former/Output_1_25", "model_0024999.pth")
        predictor = DefaultPredictor(cfg)
        meta_ultrasound = MetadataCatalog.get(dataset)
        img_filePath = os.path.join("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/images/", datasetAbre, imageName)
        #img_filePath = "/home/cf373976/Meditec/Mask2Former/dataset/CARS2/images/BL/BL - Scaphoid links - NN Daumen Abduktion (1)_096.png"
        im = cv2.imread(img_filePath)
        mask_filePath = os.path.join("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/masks/", datasetAbre, imageName)
        #mask_filePath = "/home/cf373976/Meditec/Mask2Former/dataset/CARS2/safety/masks/BL/BL - Scaphoid links - NN Daumen Abduktion (1)_096.png"
        mask = cv2.imread(mask_filePath)
        outputs = predictor(im)  # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
        v = Visualizer(im[:, :, ::-1],
            metadata=meta_ultrasound, 
            scale=1, 
            instance_mode=ColorMode.SEGMENTATION
        )

        outputs = outputs["sem_seg"].argmax(dim=0)
        out_mask = v.draw_sem_seg(outputs.to("cpu"), area_threshold=None, alpha=0.5)

        writePath = os.path.join("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/result/", datasetAbre, imageName)
        print("Write to", writePath)
        cv2.imwrite(writePath, out_mask.get_image())

        #cv2.waitKey(1000)
        #cv2.destroyAllWindows()

    # Does prediction for the test datasets and visualizes them
    def predict(self):
        datasets = ["SM", "TS"]
        for dataset in datasets:
            imagesPath = os.path.join("/home/cf373976/Meditec/Mask2Former/dataset/CARS2/images/", dataset)
            img_fileNames = os.listdir(imagesPath)
            img_fileNames.sort()
            for image in img_fileNames:
                print("Predict", os.path.join(imagesPath, image))
                self.predictSingleImage("ultrasound_train_" + dataset, dataset, image)

    # Executes prediction for all images of a dataset (currently hardcoded for the dataset "TS" and a model; just for testing!!!)
    def predictAll(self):
        # Loads configuration, sets model and executes prediction
        cfg = self.createConfiguration()
        cfg.MODEL.WEIGHTS = os.path.join("/home/cf373976/Meditec/Mask2Former/Output", "model_final.pth")
        predictor = DefaultPredictor(cfg)
        meta_ultrasound_train_TS = MetadataCatalog.get("ultrasound_train_TS")

        # Loads images and masks
        imagesPath = "/home/cf373976/Meditec/Mask2Former/dataset/CARS2/images/TS"
        img_fileNames = os.listdir(imagesPath)
        img_fileNames.sort()
        masksPath = "/home/cf373976/Meditec/Mask2Former/dataset/CARS2/safety/masks/TS"
        mask_fileNames = os.listdir(masksPath)
        mask_fileNames.sort()
        
        # Iterates through all images, executes prediction and visualizes it
        idx = 0
        for img_fileName in img_fileNames:
            im = cv2.imread(os.path.join(imagesPath, img_fileName))
            mask = cv2.imread(os.path.join(masksPath, mask_fileNames[idx]))
            outputs = predictor(im)  # format is documented at https://detectron2.readthedocs.io/tutorials/models.html#model-output-format
            v = Visualizer(im[:, :, ::-1],
                metadata=meta_ultrasound_train_TS, 
                scale=1, 
                instance_mode=ColorMode.SEGMENTATION
            )

            outputs = outputs["sem_seg"].argmax(dim=0)
            out_mask = v.draw_sem_seg(outputs.to("cpu"), area_threshold=None, alpha=0.5)

            cv2.imshow(img_fileName, out_mask.get_image()[:, :, ::-1])
            cv2.waitKey(1000)
            cv2.destroyAllWindows()

            idx += 1

    # Runs evaluation for a hardcoded dataset. Evaluates for all models and writes results into the console
    def evaluate(self):
        cfg = self.createConfiguration()
        basePath = cfg.OUTPUT_DIR
        modelList = ["model_0004999.pth", "model_0009999.pth", "model_0014999.pth", "model_0019999.pth", "model_0024999.pth", "model_0029999.pth","model_final.pth"]
        
        #for run in runsList:
        print("Evaluate and write to", basePath)
        for model in modelList:
            print("Model", model)
            cfg.MODEL.WEIGHTS = os.path.join(basePath, model)
            valDatasets = cfg.DATASETS.TEST
            predictor = DefaultPredictor(cfg)

            # With this dataset we figure out the best model. Not included into training or validation datasets
            testDataset = "ultrasound_train_SM"
            output_dir_eval = os.path.join(cfg.OUTPUT_EVAL, model)
            evaluator = SemSegEvaluator(testDataset, output_dir=output_dir_eval)
            test_loader = build_detection_test_loader(cfg, testDataset)
            print("Evaluate", testDataset, "...")
            print(inference_on_dataset(predictor.model, test_loader, evaluator))


            # Evaluate for validation datasets
            for valDataset in valDatasets:
                #output_dir_eval = os.path.join("/home/cf373976/Meditec/Mask2Former/Evaluation", run, model)
                output_dir_eval = os.path.join(cfg.OUTPUT_EVAL, model)
                evaluator = SemSegEvaluator(valDataset, output_dir=output_dir_eval)
                val_loader = build_detection_test_loader(cfg, valDataset)
                print("Evaluate", valDataset, "...")
                print(inference_on_dataset(predictor.model, val_loader, evaluator))
        print("##########################") # Seperator for better readable logs

    # Calculates the mean inference time for all test datasets (hardcoded) and all models, and writes results into logs
    #   It calculates the difference between the time after and before the prediction and calculates the total average as a result
    def meanInferenceTime(self):
        cfg = self.createConfiguration()
        basePath = "/home/cf373976/Meditec/Mask2Former"
        runsPath = cfg.OUTPUT_DIR

        # Model list
        modelList = ["model_0004999.pth", "model_0009999.pth", "model_0014999.pth", "model_0019999.pth", "model_0024999.pth", "model_0029999.pth","model_final.pth"]

        # Determine test datasets
        datasetPath = os.path.join(basePath, "dataset", "CARS2", "images")
        testDatasets = ["SM","TS", "WM"]
        
        # Iterate through all models
        for model in modelList:
            cfg.MODEL.WEIGHTS = os.path.join(runsPath, model)
            predictor = DefaultPredictor(cfg)

            # Iterates through all datasets
            for testData in testDatasets:
                print("Calculate inference time for model", model, ", dataset", testData)
                imagesPath = os.path.join(datasetPath, testData)
                img_fileNames = os.listdir(imagesPath)

                img_fileNames.sort()

                # Iterate through all images
                totalTime = 0
                for img_fileName in img_fileNames:
                    im = cv2.imread(os.path.join(imagesPath, img_fileName))      
                    startTime = time.time_ns() 
                    outputs = predictor(im)
                    stopTime = time.time_ns() 
                    totalTime += (stopTime - startTime)

                meanInferenceTimeNs = totalTime / len(img_fileNames)
                meanInferenceTimeMs = meanInferenceTimeNs / 1000000
                print("Total time for", len(img_fileNames), "images:", totalTime, "ns; ", (totalTime / 1000000), "ms")
                print("Mean inference time:", meanInferenceTimeNs, "ns (", meanInferenceTimeMs, "ms)")

    # Calculates mean inference time only for relevant models
    def meanInferenceTimeTmp(self):
        cfg = self.createConfiguration()
        basePath = "/home/cf373976/Meditec/Mask2Former"
        runsPaths = ["/home/cf373976/Meditec/Mask2Former/Output_4_25", "/home/cf373976/Meditec/Mask2Former/Output_1_25"]

        modelList = ["model_0024999.pth", "model_0029999.pth"]

        datasetPath = os.path.join(basePath, "dataset", "CARS2", "images")
        testDatasets = ["SM","TS", "WM"]
        
        # Consider models for 2 different configurations of 2 different iteration steps
        for run in runsPaths:
            for model in modelList:
                cfg.MODEL.WEIGHTS = os.path.join(run, model)
                predictor = DefaultPredictor(cfg)

                for testData in testDatasets:
                    print("Calculate inference time for model", model, "(", run, "), dataset", testData)
                    imagesPath = os.path.join(datasetPath, testData)
                    img_fileNames = os.listdir(imagesPath)

                    img_fileNames.sort()

                    totalTime = 0
                    for img_fileName in img_fileNames:
                        im = cv2.imread(os.path.join(imagesPath, img_fileName))      
                        startTime = time.time_ns() 
                        outputs = predictor(im)
                        stopTime = time.time_ns() 
                        totalTime += (stopTime - startTime)

                    meanInferenceTimeNs = totalTime / len(img_fileNames)
                    meanInferenceTimeMs = meanInferenceTimeNs / 1000000
                    print("Total time for", len(img_fileNames), "images:", totalTime, "ns; ", (totalTime / 1000000), "ms")
                    print("Mean inference time:", meanInferenceTimeNs, "ns (", meanInferenceTimeMs, "ms)")
