import os

'''
This file is used to sort all images into folders based on their first 2 letters (patient name abbreviation)
It also creates the folder if it doesn't exist
'''


basePath = '/home/cf373976/Meditec/Mask2Former/dataset/CARS2/train/masks'

fileNames = os.listdir(basePath)

for fileName in fileNames:
    oldFilePath = os.path.join(basePath, fileName)
    if os.path.isdir(oldFilePath):
        continue

    dirName = fileName[0:2]
    dirPath = os.path.join(basePath, dirName)

    if not os.path.exists(dirPath):
        os.makedirs(dirPath)

    newFilePath = os.path.join(dirPath, fileName)

    os.rename(oldFilePath, newFilePath)
